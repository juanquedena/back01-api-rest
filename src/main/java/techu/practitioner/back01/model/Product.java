package techu.practitioner.back01.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class Product {

    @JsonIgnore
    public String id;
    public String description;
    public double price;

    @JsonIgnore
    public final List<String> users = new ArrayList<>();
}
