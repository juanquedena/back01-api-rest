package techu.practitioner.back01.service.impl;

import org.springframework.stereotype.Service;
import techu.practitioner.back01.model.User;
import techu.practitioner.back01.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Service
public class UserServiceMemoryImpl implements UserService {

    ConcurrentMap<String, User> users = new ConcurrentHashMap<>();

    @Override
    public String addUser(User u) {
        users.put(u.userId, u);
        return u.userId;
    }

    @Override
    public List<User> getAllUsers() {
        return new ArrayList<>(users.values());
    }

    @Override
    public User getUser(String userId) {
        return users.get(userId);
    }

    @Override
    public void updateUser(String userId, User u) {
        users.put(userId, u);
    }

    @Override
    public void deleteUser(String userId) {
        users.remove(userId);
    }
}
