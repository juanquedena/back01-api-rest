package techu.practitioner.back01.service;

import techu.practitioner.back01.model.Product;

import java.util.List;

public interface ProductService {

    String addProduct(Product p);

    List<Product> getAllProducts();

    List<Product> getAllProductsWithoutUsers();

    Product getProduct(String id);

    void updateProduct(String id, Product p);
}
